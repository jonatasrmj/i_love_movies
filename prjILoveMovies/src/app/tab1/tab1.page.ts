import { Component } from '@angular/core';
import { TmdbService } from '../service/tmdb/tmdb.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  POSTER_URL = "https://image.tmdb.org/t/p/w500"
  filmes: any[] =[]

  constructor(
    private readonly tmbService: TmdbService
  ) {
      this.carregarFilmes()
  }
  carregarFilmes(){
    this.tmbService.buscarFilmesPeloGenero("16").subscribe({
      next: (filmes: any) => {
      console.log(filmes)
      this.filmes=filmes.results
      },
      error: (erro: any) => {
        console.error(erro)
      }
    })
  }

}
