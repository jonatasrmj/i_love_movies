import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TmdbService {
  private readonly KEY = "4debc33e73f0460b51b34ca57ec93f65"
  private readonly LANG = "pt-BR"
  private readonly URL = `https://api.themoviedb.org/3/discover/movie?api_key=${this.KEY}&language=${this.LANG}&sort_by=original_title.asc&include_adult=false&include_video=false&page=1&with_watch_monetization_types=flatrate&with_genres=`

  constructor(
    private readonly http: HttpClient
  ) {}

  buscarFilmesPeloGenero(idGenero: string): Observable<any> {
    return this.http.get<any>(`${this.URL}${idGenero}`)
  }
}
